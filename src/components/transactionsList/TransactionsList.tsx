import React, {FC, ReactElement} from 'react';
import commonStyles from "../../commonStyles";
import {applicationStore} from '../../commonTypes';
import {Transaction} from "../../classes";
import {useSelector} from "react-redux";


const TransactionsList: FC = (): ReactElement => {
    const {activeWallet} = useSelector((state: applicationStore) => state);
    const transactions = activeWallet ? activeWallet.transactions : [];
    const renderTransactions = (transactions: Transaction[]): ReactElement[] | ReactElement  => {
        if (transactions && transactions.length) {
            return transactions.map((transaction) => {
                const {fromWallet, toWallet, amount, timestamp, isIncome} = transaction;
                const transactionDate = new Date(timestamp).toLocaleString('en-GB', { timeZone: 'UTC' });
                const arrowSign = isIncome ? '←' : '→';
                return (
                    <div
                        style={{...commonStyles.transactionListItem, backgroundColor: !isIncome ? '#FFA594' : '#85FF77'}}
                    >
                            <span>{`From: ${fromWallet.walletCurrency} wallet`}</span>
                            <span>{arrowSign}</span>
                            <span>{`To: ${toWallet.walletCurrency} `}</span>
                            <span>{`Amount: ${amount} ${toWallet.walletCurrency} `}</span>
                            <span>{transactionDate}</span>
                    </div>
                );
            });
        }

        return (
            <div style={commonStyles.transactionListItemError}>
                {`Sorry, there are no transactions for ${activeWallet.walletCurrency} wallet :(`}
            </div>
        );
    };

    return (
        <div style={commonStyles.transactionList}>
            {activeWallet && renderTransactions(transactions)}
        </div>
    )
};

export default TransactionsList;
