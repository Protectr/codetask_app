import React from "react";
import WalletsCarousel from "./WalletsCarousel";
// import WalletCarouselButtons from "./WalletCarouselButtons";
// import configureStore from "redux-mock-store"; //ES6 modules
// import WalletItem from "./WalletItem";
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducer from "../../reducers";
import {shallow, mount, render} from "enzyme";


const userWalletsMock = [
  {
    walletBalance: "123",
    walletCurrency: "USD",
  },
  {
    walletBalance: "34234",
    walletCurrency: "CAD",
  },
  {
    walletBalance: "2221",
    walletCurrency: "UAH",
  },
];

const initState = {
  userWallets: userWalletsMock,
  activeWallet: userWalletsMock[1],
  targetCurrency: ["CAD", "123"],
};

describe("WalletCarousel", () => {
  const mockStore = createStore(reducer, initState);
  let getWrapperComponent;

  beforeEach(() => {
    getWrapperComponent = () =>
      shallow(
        <Provider store={mockStore}>
          <WalletsCarousel/>
        </Provider>
      );
  });

  it("Renders Wallet carousel component due to snapshot", () => {
    const component = getWrapperComponent();
    expect(component).toMatchInlineSnapshot(`
      <ContextProvider
        value={
          Object {
            "store": Object {
              "dispatch": [Function],
              "getState": [Function],
              "replaceReducer": [Function],
              "subscribe": [Function],
              Symbol(observable): [Function],
            },
            "subscription": Subscription {
              "handleChangeWrapper": [Function],
              "listeners": Object {
                "notify": [Function],
              },
              "onStateChange": [Function],
              "parentSub": undefined,
              "store": Object {
                "dispatch": [Function],
                "getState": [Function],
                "replaceReducer": [Function],
                "subscribe": [Function],
                Symbol(observable): [Function],
              },
              "unsubscribe": null,
            },
          }
        }
      >
        <WalletsCarousel />
      </ContextProvider>
    `);
  });
});
