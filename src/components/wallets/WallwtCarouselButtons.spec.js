import React from "react";
import WalletCarouselButtons from "./WalletCarouselButtons";
import {shallow, mount, render} from "enzyme";

const mockProps = {
  onSlideChange: jest.fn(),
};

let UseContextMock;

describe("WalletCarouselButtons", () => {
  let component;

  beforeEach(() => {
    UseContextMock = React.useContext = jest.fn();
    component = shallow(
      <WalletCarouselButtons onSlideChange={mockProps.onSlideChange}/>
    );
  });

  it("should render WalletCarouselButtons component", () => {
    component = shallow(<WalletCarouselButtons/>);
    expect(component).toMatchInlineSnapshot(`
      <div
        style={
          Object {
            "display": "flex",
            "justifyContent": "space-between",
            "left": 0,
            "margin": "auto",
            "position": "absolute",
            "right": 0,
            "top": "60%",
            "width": "80%",
          }
        }
      >
        <i
          style={
            Object {
              "background": "none",
              "border": "none",
              "color": "#fff",
              "fontSize": 50,
              "outline": "none",
            }
          }
        >
          &lt;
        </i>
        <i
          style={
            Object {
              "background": "none",
              "border": "none",
              "color": "#fff",
              "fontSize": 50,
              "outline": "none",
            }
          }
        >
          &gt;
        </i>
      </div>
    `);
  });


  // it("should trigger callback on ButtonBack click", () => {
  //   const btn = component.find(ButtonBack).first();
  //   btn.simulate('click');
  //   expect(mockProps.onSlideChange).toBeCalled();
  // });


});
