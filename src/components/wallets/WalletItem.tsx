import React, {FC, ReactElement} from 'react';
import commonStyles from "../../commonStyles";


const WalletsCarousel: FC<any> = ({wallet}): ReactElement => {
    return (
        <div style={commonStyles.slider}>
            <b style={commonStyles.sliderTxt}>{'Your wallet balance:'} {wallet.walletBalance}</b>
            <span style={commonStyles.sliderTxt}>{`Your wallet currency: ${wallet.walletCurrency}`}</span>
        </div>
    )
};

export default WalletsCarousel;
