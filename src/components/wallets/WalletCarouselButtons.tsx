import React, {FC, ReactElement, useContext, useEffect} from 'react';
import {ButtonBack, ButtonNext, CarouselContext,} from 'pure-react-carousel';
import commonStyles from "../../commonStyles";
import {WalletsCarouselButtonsParam} from '../../commonTypes';

const WalletsCarouselButtons: FC<WalletsCarouselButtonsParam> = ({onSliderChange}): ReactElement => {
    const carouselContext = useContext(CarouselContext);

    function onChange() {
        onSliderChange(carouselContext.state.currentSlide);
    }

    useEffect(() => {
        carouselContext.subscribe(onChange);
        return () => carouselContext.unsubscribe(onChange);
    }, []);

    return (
        <div style={commonStyles.sliderBtnHolder}>
            <ButtonBack style={commonStyles.sliderBtn}>{'<'}</ButtonBack>
            <ButtonNext style={commonStyles.sliderBtn}>{'>'}</ButtonNext>
        </div>
    );
};

export default WalletsCarouselButtons;
