import React, {FC, ReactElement, ReactNode, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {applicationStore} from '../../commonTypes';
import {Wallet} from "../../classes";
import WalletCarouselButtons from "./WalletCarouselButtons";
import {CarouselProvider, Slider, Slide} from 'pure-react-carousel';
import * as actions from "../../actions";
import "pure-react-carousel/dist/react-carousel.es.css";
import commonStyles from '../../commonStyles';
import {useDispatch} from "react-redux";
import WalletItem from './WalletItem';

const WalletsCarousel: FC = (): ReactElement => {
    const dispatch = useDispatch();
    const userWallets = useSelector((state: applicationStore) => state.userWallets);
    const [sliderState, updateSliderState] = useState({
        isSliderReady: false,
        currentSlide: 0,
    });

    const onSliderChange = (index: number): void => {
        updateSliderState({
            ...sliderState,
            currentSlide: index
        });
        dispatch(actions.updateActiveWallet(userWallets[index]));
    };

    useEffect(() => {
        if (userWallets) {
            updateSliderState({...sliderState, isSliderReady: true});
        }
    }, []);


    const renderWalletItems = (userWallets: Wallet[]): ReactNode => {
        return userWallets.map((cur, i) => (
            <Slide style={commonStyles.slide} index={i} key={cur.walletId}>
                <WalletItem wallet={cur}/>
            </Slide>
        ));
    };

    return (
        <div style={commonStyles.slider}>
            {userWallets && userWallets.length > 0 &&
                <CarouselProvider
                    visibleSlides={1}
                    totalSlides={userWallets.length}
                    step={1}
                    infinite={true}
                    naturalSlideWidth={100}
                    naturalSlideHeight={20}
                >
                    <Slider>
                        {renderWalletItems(userWallets)}
                    </Slider>
                    {/*tracking the current slide index using carousel context*/}
                    {/*using such approach due to lack on slide change callback implementation in carousel*/}
                    {/*to track current active wallet*/}
                    <WalletCarouselButtons onSliderChange={onSliderChange}/>
                </CarouselProvider>
            }
        </div>
    )
};


export default WalletsCarousel;
