import React from "react";
import WalletItem from "./WalletItem";
import { shallow } from 'enzyme';

const walletItemMock = {
  walletBalance: "123",
  walletCurrency: "USD",
};

describe("WalletItem", () => {
  let component;
  const setupComponent = (walletItemMock) => shallow(<WalletItem wallet={walletItemMock}/>);

  beforeEach(() => {
    component = setupComponent(walletItemMock);
  });

  it ("should properly render wallet item", () => {
    const spanFields = component.find("span");
    expect(spanFields.length).toBe(1);
  });

  it ("should show proper value in first span", () => {
    const spanFields = component.find("b");
    expect(spanFields.text()).toBe("Your wallet balance: 123");
  });

});


