import React, {FC, ReactElement, useState, useEffect, Fragment} from 'react';
import Modal from 'react-modal';
import * as actions from "../../actions";
import styles from "../../commonStyles";
import Loading from "../common/Loading";
import WalletCarousel from "../wallets/WalletsCarousel";
import {Wallet} from "../../classes";
import getUserWallets from "../../serviceStubs/userWalletsStub";
import {useDispatch} from "react-redux";
import {UserWalletsApiResponce} from "../../commonTypes";
import TransactionsList from "../transactionsList/TransactionsList";
import ExchangeForm from "../exchangeForm/ExchangeForm";

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement('#root');

const MainScreen: FC = (): ReactElement => {
    const dispatch = useDispatch();
    const [mainScreenState, updateMainScreenState] = useState({
        isPageReady: false,
        isModalReady: false,
        isTransactionFormModalOpen: false,
        isCurrencyPickerModalOpen: false,
    });

    function populateStoreWithWallets(data: Array<UserWalletsApiResponce>): void {
        const userWallets = data.map((cur) => {
            const {walletCurrency, walletBalance} = cur;
            return new Wallet(walletCurrency, walletBalance);
        });
        dispatch(actions.updateUserWallets(userWallets));
        dispatch(actions.updateActiveWallet(userWallets[0]));
    }

    useEffect(() => {
        getUserWallets().then((data) => {
            updateMainScreenState({...mainScreenState, isPageReady: true});
            // We get sample user wallet data from service mock
            // and then populate app store with user wallets of class Wallet
            populateStoreWithWallets(data);
        }).catch((e) => {
            console.log(`An error emerged :  ${e}`);
        });
    }, []);

    const toggleTransactionFormModal = () => {
        updateMainScreenState({
            ...mainScreenState,
            isTransactionFormModalOpen: !mainScreenState.isTransactionFormModalOpen,
        });
        dispatch(actions.updateSelectedTargetCurrency([]));
    };

    const renderItems = () => {
        if (mainScreenState.isPageReady) {
            return (
                <Fragment>
                    <WalletCarousel/>
                    <div>
                        <button
                            style={styles.commonButton}
                            onClick={() => toggleTransactionFormModal()}
                        >
                            {'Make transaction'}
                        </button>
                    </div>
                    <TransactionsList/>
                        <Modal
                            isOpen={mainScreenState.isTransactionFormModalOpen}
                            onRequestClose={() => toggleTransactionFormModal()}
                            style={customStyles}
                            contentLabel="Transaction Form Modal"
                            shouldCloseOnOverlayClick={true}
                        >
                            <ExchangeForm/>
                        </Modal>
                </Fragment>
            )
        }
        return <Loading/>
    };

    return (
        <div style={styles.flexWrapper}>
            {renderItems()}
        </div>
    )
};

export default MainScreen;
