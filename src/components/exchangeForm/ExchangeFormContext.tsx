import React from "react";

const ExchangeFormContext = React.createContext({
    targetCurrency: [],
    setTargetCurrency: (targetCurrency: []): void => {}
});
export default ExchangeFormContext;
