import React from "react";
import ExchangeForm from "./ExchangeForm";
import { shallow, mount } from 'enzyme';
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducer from "../../reducers";

const initState = {
  fromAmount: '',
  toAmount: '',
  isCurrencyPickerOpened: false,
  targetCurrency: [],
  targetWallet: undefined,
};

describe("ExchangeForm general tests", () => {
  const mockStore = createStore(reducer, initState);
  let getComponentWrapper;

  beforeEach(() => {
    getComponentWrapper = () => shallow(
      <Provider store={mockStore}>
        <ExchangeForm/>
      </Provider>
    );
  });

  // it('triggers calculate calculateConversion method on change', () => {
  //   console.log(getComponentWrapper.instance());
  // });

  it("matches the snapshot", () => {
    expect(getComponentWrapper()).toMatchSnapshot();
  });
});

