import React, {FC, ReactElement, useState, useEffect} from 'react';
import styles from "../../commonStyles";
import {useDispatch, useSelector} from 'react-redux';
import {applicationStore} from "../../commonTypes";
import {Wallet} from "../../classes";
import * as actions from "../../actions";
import CurrencyList from "../currency/CurrencyList";
import ExchangeFormContext from "./ExchangeFormContext";
import {exchangeFormInitialStateType} from "../../commonTypes";


const ExchangeForm: FC = (): ReactElement => {
    const dispatch = useDispatch();
    const state = useSelector((state: applicationStore) => state);
    const {activeWallet, userWallets} = state;
    const initialState: exchangeFormInitialStateType = {
        fromAmount: '',
        toAmount: '',
        isCurrencyPickerOpened: false,
        targetCurrency: [],
        targetWallet: undefined,
    };
    const [exchangeFormState, setExchangeFormState] = useState(initialState);
    const startTransaction = (activeWallet: Wallet): void => {
        const targetWallet = exchangeFormState.targetWallet;
        if (!targetWallet) return;

        const currentAmount = Number(activeWallet.walletBalance);
        const fromAmount = Number(exchangeFormState.fromAmount);
        const toAmount = Number(exchangeFormState.toAmount);
        if (currentAmount >= fromAmount )   {
            activeWallet.makeTransaction(targetWallet, fromAmount, toAmount);
            dispatch(actions.updateWallet(activeWallet));
        } else {
            alert("You don't have enough money on current wallet!");
        }
    };
    const toggleCurrencyPicker = (param?: boolean): void => {
        setExchangeFormState({
            ...exchangeFormState,
            isCurrencyPickerOpened: param ? param : !exchangeFormState.isCurrencyPickerOpened,
        });
    };
    const matchAndProcessInput = (val: string): boolean => {
        if (val === "") {
            setExchangeFormState({
                ...exchangeFormState,
                fromAmount: '',
                toAmount: '',
            });
            return false;
        }
        return !!(!val || val.match(/^\d{1,}(\.\d{0,2})?$/));
    };

    const calculateConversion = (val: string, isFromConversion: boolean): void => {
        let targetCurrency = exchangeFormState.targetCurrency;

        if (!(matchAndProcessInput(val) && targetCurrency.length)) {
            return;
        }
        if (!isFromConversion) {
            let converted = targetCurrency[1] ?
                (parseFloat(val) / parseFloat(targetCurrency[1])).toFixed(2) : 0;
            setExchangeFormState({
                ...exchangeFormState,
                toAmount: val,
                fromAmount: converted.toString(),
            });
        } else {
            let converted = targetCurrency[1] ? (parseFloat(targetCurrency[1]) * parseFloat(val)).toFixed(2) : 0;
            setExchangeFormState({
                ...exchangeFormState,
                fromAmount: val,
                toAmount: converted.toString(),
            });
        }
    };

    useEffect(() => {
        const {fromAmount, targetCurrency} = exchangeFormState;
        targetCurrency && targetCurrency.length &&
            setExchangeFormState({
                ...exchangeFormState,
                targetWallet: userWallets.find(w => w.walletCurrency === targetCurrency[0]),
            });
        fromAmount && calculateConversion(fromAmount, true);
    },[exchangeFormState.targetCurrency]);

    return (
        <ExchangeFormContext.Provider value={{
            targetCurrency: [],
            setTargetCurrency: (targetCurrency: []): void => {
                setExchangeFormState({
                    ...exchangeFormState,
                    targetCurrency
                });
            }
        }}>
            <div style={styles.exchangeForm}>
                <div style={styles.exchangeFormRow}>
                    <button style={styles.commonButton} onClick={() => toggleCurrencyPicker()}>
                        {'Pick up target currency'}
                    </button>
                </div>
                <div style={styles.exchangeFormRow}>
                    <label style={styles.exchangeFormLabel}>
                        {`From: ${activeWallet.walletCurrency}`}
                    </label>
                    <input
                        placeholder={`Amount in ${activeWallet.walletCurrency}`}
                        name={'fromAmount'}
                        type="text"
                        style={styles.commonFormInput}
                        onChange={(e) =>
                            calculateConversion(e.target.value, true)}
                        value={exchangeFormState.fromAmount}
                        disabled = {!(exchangeFormState.targetCurrency && exchangeFormState.targetCurrency.length)}
                    />
                </div>
                <div style={styles.exchangeFormRow}>
                    <label style={styles.exchangeFormLabel}>
                        {`To: ${exchangeFormState.targetCurrency[1] || ''}  ${exchangeFormState.targetCurrency[0] || ''}`}
                    </label>
                    <input
                        style={styles.commonFormInput}
                        name={'ToAmount'}
                        type="text"
                        value={exchangeFormState.toAmount}
                        onChange={(e) =>
                            calculateConversion(e.target.value, false)}
                        disabled = {!(exchangeFormState.targetCurrency  && exchangeFormState.targetCurrency.length)}
                    />
                </div>
                { exchangeFormState.targetCurrency &&
                    !!exchangeFormState.targetCurrency.length
                    && exchangeFormState.fromAmount
                    && exchangeFormState.toAmount &&
                    <button
                        style={styles.commonButton}
                        onClick={(e) => startTransaction(activeWallet)}>
                        {'Make transaction'}
                    </button>
                }
            </div>
            {exchangeFormState.isCurrencyPickerOpened && <CurrencyList/>}
        </ExchangeFormContext.Provider>
    )
};

export default ExchangeForm;
