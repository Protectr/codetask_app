const styles = {
  loadingWrap: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100vw',
    display: "flex",
  },
  loadingWrapGif: {
    margin: '0 auto',
    width: 150,
    display: "block",
  }
};

export default styles;
