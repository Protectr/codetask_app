import React from 'react';
import loadingGif from'../../images/loading.gif';
import loadingStyles from "./loading.styles";

const Loading = () => {
    return (
        <div style={loadingStyles.loadingWrap} >
            <img style={loadingStyles.loadingWrapGif}  src={loadingGif}  alt="Loading, please wait !"/>
        </div>
    );
};

export default Loading;
