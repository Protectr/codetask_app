import React, {FC, ReactElement, useContext} from 'react';
import styles from "../../commonStyles";
import {applicationStore, currencyListItemType} from "../../commonTypes";
import {useSelector} from "react-redux";
import ExchangeFormContext from "../exchangeForm/ExchangeFormContext";


const CurrencyListItem: FC<currencyListItemType> = ({cur}): ReactElement => {
    const state = useSelector((state: applicationStore) => state);
    const { setTargetCurrency } = useContext(ExchangeFormContext);
    const userWallets = state.userWallets;
    const currentWalletCurrency = state.activeWallet.walletCurrency;
    const isCurrencyAvailable = userWallets.some(wallet => wallet.walletCurrency === cur[0]);

    const selectTargetCurrency = (currency: any): void => {
        isCurrencyAvailable && setTargetCurrency(currency);
    };

    const renderListItem = () => {
        if (currentWalletCurrency === cur[0]) return <div></div>;
        return (
                <div
                    style={{...styles.currencySearchItem, backgroundColor: !isCurrencyAvailable ? "#ffb369" : "#b5edf1" }}
                    onClick={() => selectTargetCurrency(cur)}
                >
                    <span>{cur[0]}</span>
                    <span>{cur[1]}</span>
                    {!isCurrencyAvailable && <span>No wallet of such currency</span>}
                </div>
        );
    };

    return (
        renderListItem()
    );
};

export default CurrencyListItem;
