import React, {FC, ReactElement} from 'react';
import {CurrencySearchStateParam} from '../../commonTypes';
import styles from "../../commonStyles";

const CurrencyList: FC<CurrencySearchStateParam> = ({onCurrencySearchInputChange}): ReactElement => {
    return (
        <div style={styles.currencySearch}>
            <input
                style={styles.commonFormInput}
                placeholder={'Start typing currency name'}
                type={'text'}
                onChange={(e) => onCurrencySearchInputChange(e.target.value)}
            />
        </div>
    );
};

export default CurrencyList;
