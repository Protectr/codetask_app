import React, {FC, ReactElement, useState, useEffect} from 'react';
import CurrencySearch from './CurrencySearch';
import CurrencyListItem from "./CurrencyListItem";
import currencyService from "../../services";
import {applicationStore, currencyApiResponce} from "../../commonTypes";
import styles from "../../commonStyles";
import {useSelector} from "react-redux";
import loadingStyles from "../common/loading.styles";
import loadingGif from "../../images/loading.gif";


const CurrencyList: FC = (): ReactElement => {
    const [currencyListState, setCurrencyListState] = useState({
        curString: '',
        rates: {},
        filteredRates: {},
    });

    const state = useSelector((state: applicationStore) => state);
    const currentWalletCurrency = (state.activeWallet)
        ? state.activeWallet.walletCurrency : "";

    const filterRates = (s: string, rates: object) => {
        s = s.toLowerCase();
        if (!s) return rates;
        return Object.entries(rates).reduce((acc: any, cur) => {
            const key = cur[0];
            if (key.toLowerCase().includes(s)) {
                acc[key] = cur[1];
                return acc;
            }
            return acc;
        }, {});
    };

    const onCurrencySearchInputChange = (s: any): void => {
        setCurrencyListState({
            ...currencyListState,
            curString: s,
            filteredRates: filterRates(s, currencyListState.rates),
        });
    };

    const renderCurItems = (rates: object) => {
        return Object.entries(rates).map(cur => {
                return (
                    <CurrencyListItem
                        key={'_'+cur[0]+cur[1]}
                        cur={cur}
                    />
                )
            }
        );
    };

    useEffect(() => {
        const subscription = currencyService.currencyLiveObservable(currentWalletCurrency).subscribe((result: currencyApiResponce) => {
            setCurrencyListState({
                ...currencyListState,
                rates: result.rates,
                filteredRates: filterRates(currencyListState.curString, result.rates)
            });
        });
        return () => {
            subscription.unsubscribe()
        };
    }, [currencyListState, state.userWallets]);

    return (
        <div style={styles.currencyList}>
            <CurrencySearch onCurrencySearchInputChange={onCurrencySearchInputChange}/>
            <div style={styles.currencyListWrap}>
                {
                    Object.entries(currencyListState.rates).length
                        ? renderCurItems(currencyListState.filteredRates)
                        : <img style={loadingStyles.loadingWrapGif}  src={loadingGif}  alt="Loading, please wait !"/>
                }
            </div>
        </div>
    );
};

export default CurrencyList;
