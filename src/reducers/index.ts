import actionTypes from '../actionTypes';
import {Action} from '../commonTypes';
import {Wallet} from "../classes";


const initialState = {
    userWallets: [],
    activeWallet: {},
    targetCurrency: [],
};

const reducer = (state = initialState, {type, payload}: Action) => {
    switch (type) {
        case actionTypes.UPDATE_USER_WALLETS:
            return {
                ...state,
                userWallets: payload,
            };
        case actionTypes.UPDATE_SELECTED_TARGET_CURRENCY:
            return {
                ...state,
                targetCurrency: payload,
            };
        case actionTypes.UPDATE_ACTIVE_WALLET:
            return {
                ...state,
                activeWallet: payload,
            };
        case actionTypes.UPDATE_WALLET:
            const updateUserWallet = (userWallets: Wallet[], payload: Wallet) => {
                return userWallets.map((cur) => {
                    if (cur.walletId === payload.walletId) {
                        cur = payload;
                    }
                    return cur;
                });
            };

            return {
                ...state,
                userWallets: [...updateUserWallet(state.userWallets, payload)],
            };
        default:
            return state;
    }
};

export default reducer;
