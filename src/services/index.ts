import { interval } from 'rxjs';
import { switchMap} from 'rxjs/operators';


class CurrencyService {
    endpoint: string = "https://api.exchangeratesapi.io/latest";

    getCurrencyRatesPromise(base?: string): Promise<Response> {
        return fetch(`${this.endpoint}?base=${base}`);
    };

    currencyLiveObservable(base?: string) {
        return interval(1500)
            .pipe(
                switchMap(() => this.getCurrencyRatesPromise(base)),
                switchMap((response: Response) => response.json())
            )
    };
}

const currencyService = new CurrencyService();
export default currencyService;
