class Wallet {
    walletCurrency: string;
    walletId: string;
    walletBalance: number;

    constructor(currency: string, initWalletBalance = 0) {
        this.walletCurrency = currency;
        this.walletBalance = initWalletBalance;
        this.walletId = this.getUniqueWalletId();
    }

    getUniqueWalletId(): string {
        const randomKey = Math.random().toString(36).substr(2, 9);
        return `wallet_${this.walletCurrency}_${randomKey}`;
    }

    transactions: Transaction[] = [];

    subtractBalance(amount: number): void {
        this.walletBalance = this.walletBalance - amount;
    }

    addBalance(amount: number): void {
        this.walletBalance = this.walletBalance + amount;
    }

    makeTransaction(toWallet: Wallet, fromAmount: number, toAmount: number): void {
        this.subtractBalance(fromAmount);
        toWallet.addBalance(toAmount);
        const fromTransaction = new Transaction(this, toWallet, fromAmount);
        const toTransaction = new Transaction(this, toWallet, toAmount, true);
        this.transactions.push(fromTransaction);
        toWallet.transactions.push(toTransaction);
    }
}

class Transaction {
    fromWallet: Wallet;
    toWallet: Wallet;
    amount: number;
    timestamp: number;
    isIncome: boolean | undefined;

    constructor(walletFrom: Wallet, walletTo: Wallet, transferAmount: number, isIncomeTransaction?: boolean) {
        this.fromWallet = walletFrom;
        this.toWallet = walletTo;
        this.amount = transferAmount;
        this.timestamp = Date.now();
        this.isIncome = isIncomeTransaction;
    }
}


export {Wallet, Transaction}
