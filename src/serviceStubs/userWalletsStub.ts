const getUserWallets = (): Promise<any[]> => {
  const walletData = [
    {
      walletCurrency: 'USD',
      walletBalance: 1234.55,
    },
    {
      walletCurrency: 'CAD',
      walletBalance: 1234.55,
    },
    {
      walletCurrency: 'HKD',
      walletBalance: 1234.55,
    },
    {
      walletCurrency: 'EUR',
      walletBalance: 2234.55,
    },
    {
      walletCurrency: 'GBP',
      walletBalance: 777,
    }
  ];

  return Promise.resolve(walletData);

};

export default getUserWallets;
