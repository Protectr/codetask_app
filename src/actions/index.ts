import actionTypes from '../actionTypes';
import {Wallet} from "../classes";
import {currencyListItemType} from "../commonTypes";

const updateUserWallets = (payload: Wallet[]) => {
    return {
        type: actionTypes.UPDATE_USER_WALLETS,
        payload
    }
};

const updateActiveWallet = (payload: Wallet) => {
    return {
        type: actionTypes.UPDATE_ACTIVE_WALLET,
        payload
    }
};

const updateWallet = (payload: Wallet) => {
    return {
        type: actionTypes.UPDATE_WALLET,
        payload
    }
};


const updateSelectedTargetCurrency = (payload: currencyListItemType | Array<string>) => {
    return {
        type: actionTypes.UPDATE_SELECTED_TARGET_CURRENCY,
        payload
    }
};

export {updateUserWallets, updateActiveWallet, updateWallet, updateSelectedTargetCurrency};
