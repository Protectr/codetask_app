import {Wallet} from "../classes";
import {CSSProperties} from 'react';

interface StylesDictionary {
    [Key: string]: CSSProperties;
};

interface UserWalletsApiResponce {
    walletIsActive: boolean,
    walletCurrency: string,
    walletBalance: number,
}

interface Action {
    type: string,
    payload: any
}

interface applicationStore {
    userWallets: Wallet[],
    activeWallet: Wallet,
    targetCurrency: Array<string>,
}

interface WalletsCarouselButtonsParam {
    onSliderChange: (index: number) => void,
}

interface CurrencySearchStateParam {
    onCurrencySearchInputChange: (s: string) => void,
}

interface toggleCurrencyPickerModalParam {
    toggleCurrencyPickerModal: () => void,
}

interface currencyApiResponce {
    base: string,
    url: string,
    rates: object
}

interface currencyListItemType {
    cur: Array<string>
}

interface exchangeFormInitialStateType {
    fromAmount: string,
    toAmount: string,
    isCurrencyPickerOpened: boolean,
    targetCurrency: string[],
    targetWallet: undefined | Wallet,
}

export type {
    exchangeFormInitialStateType,
    currencyListItemType,
    UserWalletsApiResponce,
    Action,
    applicationStore,
    StylesDictionary,
    WalletsCarouselButtonsParam,
    CurrencySearchStateParam,
    currencyApiResponce,
    toggleCurrencyPickerModalParam
}
