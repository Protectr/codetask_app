import {StylesDictionary} from "../commonTypes";

const commonStyles: StylesDictionary = {
  flexWrapper: {
    display: 'flex',
    minHeight: '100vh',
    color: '#fff',
    backgroundColor: "#459eb7",
    flexDirection: "column",
  },
  slide: {
    display: "flex",
    alignItems: 'center',
    justifyContent: 'center',
  },
  slideCur: {},
  slideBalance: {},
  slideActive: {},
  sliderInner: {
    borderWidth: 1,
    textAlign: "center",
    borderColor: "#fff",
    padding: 10,
    width: "80%",
  },
  slider: {
    margin: '0 auto',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    paddingLeft: 30,
    paddingRight: 30,
    boxSizing: "border-box",
    paddingTop: 30,
    textAlign: "center",
  },
  sliderTxt: {
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 10,
  },
  sliderBtnHolder: {
    width: '80%',
    margin: "auto",
    display: 'flex',
    justifyContent: 'space-between',
    position: "absolute",
    top: '60%',
    left: 0,
    right: 0,
  },
  sliderBtn: {
    outline: "none",
    background: "none",
    border: "none",
    fontSize: 50,
    color: '#fff',
  },
  transactionList: {
    backgroundColor: "#fff",
    flexGrow: 2,
    paddingTop: 10,
    paddingBottom: 10,
  },

  transactionListItem: {
    boxSizing: "border-box",
    color: '#3378B7',
    borderBottomColor:  "#3378B7",
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#FFA594',
    justifyContent: "space-between",
    display: 'flex',
  },
  transactionListItemError: {
    boxSizing: "border-box",
    color: '#851618',
    borderBottomColor:  "#3378B7",
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 10,
    paddingRight: 10,
  },
  currencyList: {
    maxHeight: '40vh',
  },
  currencyListWrap: {
    maxHeight: '30vh',
    overflowY: 'scroll',
  },
  currencySearch: {
    display: "flex",
    textAlign: "center",
    flexDirection: "column",
    marginBottom: 10,
  },
  currencySearchItem: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
    marginBottom: 10,
  },
  exchangeForm: {
    textAlign: 'center',
    backgroundColor: '#7ab7b0',
    padding: 10,
    marginBottom: 5,
    width: '60vw',
  },
  exchangeFormLabel: {
    marginBottom: 5,
    display: "inline-block",
    color: '#fff',
  },
  exchangeFormRow: {
    padding: 5,
    marginBottom: 5,
  },
  commonFormInput: {
    outline: 'none',
    color: "#2798ff",
    fontSize: 20,
    minHeight: 40,
    width: '100%',
    boxSizing: "border-box",
    marginTop: 5,
  },
  commonButton: {
    outline: 'none',
    color: "#fff",
    fontSize: 20,
    minHeight: 40,
    width: '100%',
    backgroundColor: "#2798ff",
    border: "none",
    cursor: "pointer",
    boxSizing: "border-box",
  },
};

export default commonStyles;
